package com.example.ankir.notificairplane;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

/**
 * Created by ankir on 06.06.2017.
 */

public class ReciverAirPlane extends BroadcastReceiver {

    public static final int NOTIFY_ID = 100;
    public static final int ID = 200;
    String state;

    @Override
    public void onReceive(Context context, Intent intent) {

        long[] vibrate;
        sendMessage(context, intent);
        if (isBluetoothEnabled()) {
            state = "ББ уже на кухне";
            vibrate = new long[]{100, 500, 500, 500, 500, 1000};
        } else if (isWiFiEnabled(context)) {
            state = "Ты под колпаком";
            vibrate = new long[]{100, 2000};
        } else {
            state = "Большой брат следит за тобой";
            vibrate = new long[]{100, 1000};
        }

        Intent notificationIntent = new Intent(context, MainActivity.class);
        notificationIntent.addFlags(FLAG_ACTIVITY_NEW_TASK);


        PendingIntent contentIntent = PendingIntent.getActivity(context,
                ID, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (!isAirplaneModeOn(context)) {
            Notification.Builder builder = new Notification.Builder(context);
            builder.setContentIntent(contentIntent)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setAutoCancel(true)
                    .setContentTitle("WARNING!!!")
                    .setVibrate(vibrate)
                    .setContentText(state);

            Notification n = builder.build();
            nm.notify(NOTIFY_ID, n);
        } else nm.cancel(NOTIFY_ID);
    }

    private static boolean isAirplaneModeOn(Context context) {
        return Settings.Global.getInt(context.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
    }

    private boolean isWiFiEnabled(Context context) {
        ConnectivityManager cm = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
    }

    private boolean isBluetoothEnabled() {
        int stateBluetooth = BluetoothAdapter.getDefaultAdapter().getState();
        return stateBluetooth == BluetoothAdapter.STATE_ON;
    }

    public void sendMessage(Context context, Intent intent) {
        //  Intent intent = new Intent();
        intent.setAction(MainActivity.BROADCAST_ACTION);
        LocalBroadcastManager localBM = LocalBroadcastManager.getInstance(context);
        intent.putExtra(MainActivity.AIR_KEY, isAirplaneModeOn(context) ? "On" : "Off");
        intent.putExtra(MainActivity.WIFI_KEY, isWiFiEnabled(context) ? "On" : "Off");
        intent.putExtra(MainActivity.BLUETOOTH_KEY, isBluetoothEnabled() ? "On" : "Off");
        localBM.sendBroadcast(intent);
    }
}