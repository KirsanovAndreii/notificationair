package com.example.ankir.notificairplane;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    public static final String BROADCAST_ACTION = "com.example.ankir.notificairplane.state";
    public static final String AIR_KEY = "air";
    public static final String WIFI_KEY = "wifi";
    public static final String BLUETOOTH_KEY = "bluetooth";

    BroadcastReceiver localBroadcastRecyver;

    @BindView(R.id.state_air)
    TextView stateAirPlane;
    @BindView(R.id.state_wifi)
    TextView stateWiFi;
    @BindView(R.id.state_bluetooth)
    TextView stateBluetooth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        localBroadcastRecyver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String airState = intent.getStringExtra(AIR_KEY);
                String wifiState = intent.getStringExtra(WIFI_KEY);
                String bluetoothState = intent.getStringExtra(BLUETOOTH_KEY);
                stateAirPlane.setText(airState);
                stateWiFi.setText(wifiState);
                stateBluetooth.setText(bluetoothState);

            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(localBroadcastRecyver, intentFilter);

    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(localBroadcastRecyver);
    }
}
